package b19_phuong_trinh;

import java.util.Scanner;

public class PhuongTrinh {
    private static final Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("PT Bac Nhat");
        System.out.println("Nhap a: ");
        int a = sc.nextInt();
        System.out.println("Nhap b: ");
        int b = sc.nextInt();
        giaiPTBac1(a, b);

        System.out.println("PT Bac Hai");
        System.out.println("Nhap a: ");
        a = sc.nextInt();
        System.out.println("Nhap b: ");
        b = sc.nextInt();
        System.out.println("Nhap c: ");
        int c = sc.nextInt();
        giaiPTBac2(a, b, c);
    }

    private static void giaiPTBac1 (float a, float b) {
        // ax + b = 0
        if (a == 0) {
            if (b == 0) {
                System.out.println("PT vo so nghiem");
            } else {
                System.out.println("PT vo nghiem");
            }
        } else {
            System.out.println("PT co nghiem: " + (-b / a));
        }
    }

    private static void giaiPTBac2 (float a, float b, float c) {
        // ax^2 + bx + c =0
        if (a == 0) {
            giaiPTBac1(b, c);
            return;
        }
        float x1, x2;
        float delta = (float) (Math.pow(b, 2) - 4 * a * c);
        if (delta < 0) {
            System.out.println("PT vo nghiem");
        } else if (delta == 0) {
            x1 = -b / (2 * a);
            System.out.println("Phuong trinh co 1 nghiem la x1 = x2 = " + x1);
        } else {
            x1 = (float) ((-b + Math.sqrt(delta)) / (2 * a));
            x2 = (float) ((-b - Math.sqrt(delta)) / (2 * a));
            System.out.println("Phương trình có 2 nghiệm x1 = " + x1 + " và x2 = " + x2);
        }
    }
}
