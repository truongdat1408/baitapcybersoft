package b18_so_nam_cho_doi;

import java.text.DecimalFormat;
import java.util.Scanner;

public class SoNamChoDoi {

    private static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        /*
        Input: so tien, so lai, so tien muon co
        Output: so nam co tien
         */
        String patternTien = "#,### dong";
        DecimalFormat dc = new DecimalFormat(patternTien);

        System.out.println("Nhap so tien ban co: ");
        double tienBanDau = sc.nextDouble();
        System.out.println("Nhap so tien ban muon co: ");
        double tienMongMuon= sc.nextDouble();
        System.out.println("Nhap so lai suat ngan hang: ");
        float laiSuat = sc.nextFloat();

        double tienLai = 0;
        int soNam = 0;
        while (tienBanDau < tienMongMuon) {
            tienLai = tienBanDau * laiSuat / 100;
            tienBanDau += tienLai;
            soNam++;
        }

        System.out.printf("Sau %d nam ban se nhan duoc so tien mong muon: " + dc.format(tienBanDau), soNam);
    }
}
