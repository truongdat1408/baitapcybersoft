package b22_palindromic;

import java.util.Scanner;

public class Palindromic {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap chuoi: ");
        String x = sc.nextLine();
        System.out.println("Longest Palindrome: " + longestPalindromeString(x));
    }

    /*
    Xét từng vị trí của chuỗi bằng con trỏ left (l) và right (r)

    121  -> so 1 la palindrome dau tien
    r
    l

    121   -> 121   -> so 121 la palindrome tiep theo
     r         r
     l       l
     */
    static public String intermediatePalindrome(String s, int left, int right) {
        if (left > right) return null;

        while (left >= 0 && right < s.length()
                && s.charAt(left) == s.charAt(right)) {
            left--;
            right++;
        }
        return s.substring(left + 1, right);
    }

    public static String longestPalindromeString(String s) {
        if (s == null) return null;
        String longest = s.substring(0, 1); // Neu nhap chuoi co 1 ki tu -> ki tu do la dai nhat

        for (int i = 0; i < s.length() - 1; i++) {
            //truong hop so le: 121
            String palindrome = intermediatePalindrome(s, i, i);
            if (palindrome.length() > longest.length()) {
                longest = palindrome;
            }
            //truong hop so chan: 1221 -> duoc chuoi rong
            //                    l
            //                     r

            //1221 -> 1221  -> 1221
            // l      l
            //  r        r
            palindrome = intermediatePalindrome(s, i, i + 1);
            if (palindrome.length() > longest.length()) {
                longest = palindrome;
            }
        }
        return longest;
    }
}