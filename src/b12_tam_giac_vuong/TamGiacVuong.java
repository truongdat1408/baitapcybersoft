package b12_tam_giac_vuong;

import java.util.Scanner;

public class TamGiacVuong {
    public static void main(String[] args) {
        int n;
        do {
            System.out.print("Nhap vao 1 so tu (1 -> 9): ");
            n = new Scanner(System.in).nextInt();
        } while (n < 1 || n > 9);

        print(n);
    }

    public static void print(int n) {
        String chuoi = "";
        for (int i = 1; i <= n; i++) {
            chuoi += i;
            System.out.println(chuoi);
        }
    }
}
