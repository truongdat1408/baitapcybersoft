package b0_karaoke;

import java.text.ParseException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class Karaoke {
    private final static Scanner scanner = new Scanner(System.in);
    private LocalTime beginTime;
    private LocalTime endTime;

    private int waterBottles;

    private double totalCost;

    private boolean isDiscount;

    private final DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm");

    public static void main(String[] args) throws ParseException {
        Karaoke k = new Karaoke();
        k.inputBill();
        k.countTotalCost();
        k.printBill();
    }

    public void inputBill () {
        do {
            //Enter Time
            System.out.print("Please enter begin time (HH:mm) (9:00 - 23:59):");
            String bTime = scanner.nextLine();
            System.out.print("Please enter end time (HH:mm) (9:00 - 23:59) (must be after Begin Time):");
            String eTime = scanner.nextLine();

            //Parse Time
            try {
                setBeginTime(LocalTime.parse(bTime, df));
                setEndTime(LocalTime.parse(eTime, df));
            } catch (Exception e) {
                System.out.println("Time you entered is wrong!");
            }

        } while (getBeginTime().isAfter(getEndTime())
                || getBeginTime().isBefore(LocalTime.parse("09:00", df))); //Only compare beginTime -> endTime okay

        //Discount if beginTime 9:00 - 24:00
        setDiscount(getBeginTime().isBefore(LocalTime.parse("17:00", df)));

        do {
            System.out.print("Water bottles: ");
            setWaterBottles(scanner.nextInt());
        } while (getWaterBottles() < 0);

        System.out.println("Counting...");
    }

    public void printBill() {
        System.out.println("============ KARAOKE BILL =============");
        System.out.println("+ Start time: " + df.format(getBeginTime()));
        System.out.println("+ End time: " + df.format(getEndTime()));
        System.out.println("+ Water Bottles: " + getWaterBottles());
        System.out.println("+ Discount: " + (isDiscount() ? "20%" : "No"));
        System.out.println("+ Cost: " + (isDiscount() ? getTotalCost() * 100 / 80 * 1.0 : getTotalCost()));
        System.out.println("+ Payment: " + getTotalCost());
    }

    private void countTotalCost () {
        if (beginTime.isAfter(endTime)) {
            System.out.println("ERROR beginTime is after endTime");
            return;
        }
        if (beginTime.isBefore(endTime)) {
            long diffSecond = Duration.between(beginTime, endTime).get(ChronoUnit.SECONDS);
            long diffMinus = diffSecond / 60;
            long diffHour = diffMinus / 60;

            if (diffMinus % 60 != 0) {
                diffHour += 1;
            }

            if (diffHour >= 0 && diffHour <= 1) {
                setTotalCost(30000);
            }
            if (diffHour > 1 && diffHour <= 3) {
                setTotalCost(30000 * diffHour);
            }
            if (diffHour > 3) {
                setTotalCost(30000*3 + 90000 * 30/100 * 1.0 * (diffHour - 3));
            }

            //Water cost
            setTotalCost(getWaterBottles()*10000 + getTotalCost());

            //Discount
            if (isDiscount()) {
                setTotalCost(getTotalCost() * 80 / 100 * 1.0);
            }
        }

    }

    public LocalTime getBeginTime() {
        return beginTime;
    }

    private void setBeginTime(LocalTime beginTime) {
        this.beginTime = beginTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    private void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public double getTotalCost() {
        return totalCost;
    }

    private void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public int getWaterBottles() {
        return waterBottles;
    }

    public void setWaterBottles(int waterBottles) {
        this.waterBottles = waterBottles;
    }

    public boolean isDiscount() {
        return isDiscount;
    }

    public void setDiscount(boolean discount) {
        isDiscount = discount;
    }
}
