package b17_game_doan_so;

import java.util.Random;
import java.util.Scanner;

public class GameDoanSo {
    private final static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        Random r = new Random();
        int soBiMat = r.nextInt(1001);

        int num;
        do {
            System.out.println("Nhap so bi mat: (1 -> 1000)");
            num = sc.nextInt();
        } while (num < 1 || num > 1000);

        while (num != soBiMat) {
            if (num < soBiMat) {
                System.out.println("So ban nhap NHO hon so bi mat");
            } else {
                System.out.println("So ban nhap LON hon so bi mat");
            }

            System.out.println("Nhap lai so bi mat:");
            num = sc.nextInt();
        }

        System.out.println("So bi mat la: " + num);
    }
}
