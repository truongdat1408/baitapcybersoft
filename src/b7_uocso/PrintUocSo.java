package b7_uocso;

import java.util.Scanner;

public class PrintUocSo {
    public static void main(String[] args) {
        // 10: 5 2 1
        System.out.print("Nhập 1 số: ");
        int number = new Scanner(System.in).nextInt();
        System.out.printf("Các ước của số %d: ", number);
        for (int i = 1; i <= number/2; i++) {
            if (number % i == 0) System.out.print(i + ", ");
        }
        System.out.println(number);
    }
}
