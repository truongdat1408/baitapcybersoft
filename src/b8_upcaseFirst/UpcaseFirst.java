package b8_upcaseFirst;

import java.util.Scanner;

public class UpcaseFirst {
    private static Scanner scanner;
    static {
        scanner = new Scanner(System.in);
    }

    public static void main(String[] args) {
        System.out.print("Nhap vao 1 chuoi: ");
        System.out.println("----> " + upcaseFirst(scanner.nextLine()));
    }

    public static String upcaseFirst(String chuoi) {
        String result = "";
        for(String x : chuoi.split(" ")) {
            // su dung substring de in hoa chu cai dau tien
            result += x.substring(0, 1).toUpperCase() + x.substring(1) + " ";
        }
        return result.trim();
    }
}
