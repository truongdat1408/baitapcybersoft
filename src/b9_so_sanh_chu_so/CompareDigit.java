package b9_so_sanh_chu_so;

import java.util.Scanner;

public class CompareDigit {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a, b;
        do {
            a = sc.nextInt();
            b = sc.nextInt();
        } while (a < 10 || a > 99 || b < 10 || b > 99);
        System.out.println("---->" + (compare(a, b) + "").toUpperCase());
    }

    public static boolean compare(int a, int b) {
        String al = a + "";
        String bl = b + "";
        for(int i = 0; i < bl.length(); i++) {
            if (al.contains(bl.charAt(i) + "")){
                return true;
            }
        }
        return false;
    }
}
