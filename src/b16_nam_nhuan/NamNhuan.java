package b16_nam_nhuan;

import java.util.Scanner;

public class NamNhuan {
    public static void main(String[] args) {
        /*
        (1) Những năm chia hết cho 4 mà không chia hết cho 100 là năm nhuận
        (2) Những năm chia hết cho 100 mà không chia hết cho 400 thì KHÔNG PHẢI là năm nhuận
        (3) Những năm chia hết đồng thời cho 100 và 400 là năm nhuận
         */
        System.out.print("Nhap nam: ");
        int nam = new Scanner(System.in).nextInt();

        /*
            12: năm nhuận
            13: năm không nhuận
            1000: năm không nhuận
            2000: năm nhuận
         */
        if (nam % 4 == 0) {
            if (nam % 100 == 0) {
                if (nam % 400 == 0) {
                    System.out.println(nam + " la nam nhuan"); // 3
                } else {
                    System.out.println(nam + " khong phai la nam nhuan"); // 2
                }
            } else {
                System.out.println(nam + " la nam nhuan"); // 1
            }
        } else {
            System.out.println(nam + " khong phai la nam nhuan");
        }
    }
}
