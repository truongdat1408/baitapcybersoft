package b5_AB_length;

import java.util.Scanner;

public class ABLength {
    private class ToaDo {
        int x;
        int y;

        @Override
        public String toString() {
            return x + ", " + y;
        }
    }

    private ToaDo A;
    private ToaDo B;

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        ABLength abLength = new ABLength();
        abLength.enterAB();
        System.out.printf("AB length: %.2f", abLength.lengthAB());
    }

    public double lengthAB() {
        return Math.sqrt(Math.pow((B.x - A.x), 2) + Math.pow((B.y - A.y), 2));
    }

    public void enterAB() {
        A = new ToaDo();
        B = new ToaDo();

        System.out.println("Enter A: ");
        System.out.print("+ x1 = ");
        A.x = scanner.nextInt();
        System.out.print("+ y1 = ");
        A.y = scanner.nextInt();

        System.out.println("Enter B: ");
        System.out.print("+ x2 = ");
        B.x = scanner.nextInt();
        System.out.print("+ y2 = ");
        B.y = scanner.nextInt();

        System.out.println("You entered: A(" + A + "), and B(" + B + ")");
    }
}
