package b21_kangaroo;

import java.util.Scanner;

public class Kangaroo {
    //Input: kan1 = x1, v2, kan2 = x2, v2 (x: vi tri, v: van toc)
    //          - dieu kien: 0 < x < 10000, 1 < x < 10000
    //Thuat toan: sau 1 lan nhay, vi tri 2 con kan tang them bang dung van toc cua tung con
    //Output: 2 con co gap nhau hay khong?

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x1, v1, x2, v2;
        double jump = 0;
        do {
            System.out.println("Kangaroo 1: ");
            System.out.println("- Vi tri: ");
            x1 = sc.nextInt();
            System.out.println("- Van toc: ");
            v1 = sc.nextInt();
        } while (0 > x1 || x1 > 10000 || 1 > v1 && v1 > 10000);

        do {
            System.out.println("Kangaroo 1: ");
            System.out.println("- Vi tri: ");
            x2 = sc.nextInt();
            System.out.println("- Van toc: ");
            v2 = sc.nextInt();
        } while (0 > x2 || x2 > 10000 || 1 > v2 && v2 > 10000);

        // 2 con gap nhau tai mot gia tri x = x1 = x2 => x1 + v1 * n = x2 + v2 * n (n la so lan nhay)
        // => x1 - x2 = (v2 - v1) * n
        // => (v2 - v1) * n + (x2 - x1) = 0
        // => giai phuong trinh bac nhat => neu n la so thuc thi lam tron len
        if (v2 - v1 == 0) {
            if (x2 - x1 == 0) {
                System.out.println("2 con gap nhau ma khong can nhay");
            } else {
                System.out.println("2 con khong the gap nhau");
            }
        } else {
            jump = -(x2 - x1) / (v2 - v1) * 1.0;
            System.out.println("2 con gap nhau sau " + Math.floor(jump) + " lan nhay, gap nhau tai vi tri: " + (x1 + v1 * jump));
        }
    }
}
