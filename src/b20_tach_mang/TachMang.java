package b20_tach_mang;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TachMang {
    private final static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Nhap vao so phan tu cua mang: ");
        int n = sc.nextInt();
        List<Integer> arr = new ArrayList<>();
        enterArray(arr, n);

        System.out.println("So phan tu chan: " + doSolution(arr, x -> x % 2 == 0));
        System.out.println("So phan tu le: " + doSolution(arr, x -> x % 2 != 0));
    }

    private static void enterArray (List<Integer> arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.printf("arr[%d] = ", i);
            arr.add(sc.nextInt());
        }
    }

    private static List<Integer> doSolution (List<Integer> arr, Predicate<? super Integer> predicate) {
        return arr.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

}
