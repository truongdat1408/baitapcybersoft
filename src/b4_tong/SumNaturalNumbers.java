package b4_tong;

public class SumNaturalNumbers {
    public static void main(String[] args) {
        System.out.println(sum(234));
    }

    // VD: 234 -> 2 + 3 + 4 = 9
    private static int sum (int num) {
        int tong = 0;
        while (num != 0) {
            tong += (num % 10);
            num /= 10;
        }
        return tong;
    }
}
