package b11_so_tu_nhien_lon_nhat;

import java.util.Scanner;

public class SoTuNhienLonNhat {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap vao mot so tu nhien: ");
        int n = sc.nextInt();
        //log2 (n) = loge (n) / loge (2)
        System.out.println("So tu nhien lon nhat nho hon log2 ("+ n +") la:" + Math.floor(Math.log(n) / Math.log(2)));
    }
}
