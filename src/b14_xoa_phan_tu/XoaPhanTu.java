package b14_xoa_phan_tu;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class XoaPhanTu {
    private final static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Nhap vao so phan tu cua mang: ");
        int n = sc.nextInt();
        List<Integer> arr = new ArrayList<>();
        enterArray(arr, n);

        //VD: 1 2 2 2 1 -> 1 2
        List<Integer> mangKhongTrungLap = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            if (!mangKhongTrungLap.contains(arr.get(i))) {
                mangKhongTrungLap.add(arr.get(i));
            }
        }

        arr.clear();
        arr.addAll(mangKhongTrungLap);

        System.out.println("Mang hien tai: " + arr.stream().collect(Collectors.toList()));
    }

    private static void enterArray (List<Integer> arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.printf("arr[%d] = ", i);
            arr.add(sc.nextInt());
        }
    }
}
