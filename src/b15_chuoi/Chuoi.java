package b15_chuoi;

import java.util.Scanner;

public class Chuoi {
    public static void main(String[] args) {
        //a: nhap chuoi -> in do dai
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap vao 1 chuoi: ");
        String chuoi = sc.nextLine();
        chuoi = chuoi.trim();
        System.out.println("Do dai chuoi: " + chuoi.length());

        //b: in ra ky tu
        System.out.print("Nhap vi tri ky tu can in ra: ");
        int index = sc.nextInt();
        System.out.println("Ky tu do la: " + chuoi.charAt(index));

        //c: abcdef
        boolean isContain = chuoi.contains("abcdef");
        System.out.println(isContain ? "Chuoi vua nhap co chuoi phu \"abcdef\"" : "Chuoi vua nhap KHONG co chuoi phu \"abcdef\"");
    }
}
