package b2_dec_to_bin;

public class ChangeDecAndBin {
    public static void main(String[] args) {
        System.out.println(new ChangeDecAndBin().DecToBin(6));
        System.out.println(new ChangeDecAndBin().BinToDec(110));
    }
    /*
    * n = 6
    * 0, 3 => 6 % 2 * 10 ^ 0
    * 1, 2 => 3 % 2 * 10 ^ 1
    * 0, 1 => 2 % 2 * 10 ^ 2
    * */
    public long DecToBin (int decNumber) {
        long binNum = 0;
        int i = 0;
        while (decNumber > 0) {
            binNum += (decNumber % 2) * Math.pow(10, i);
            decNumber /= 2;
            ++i;
        }
        return binNum;
    }

    public int BinToDec (long binNumber) {
        int decNum = 0;
        int i = 0;
        //6: 110 = 1 * 2^2 + 1 * 2^1 + 0 * 2^0
        while (binNumber > 0) {
            decNum += (binNumber % 10) * Math.pow(2, i);
            i++;
            binNumber /= 10;
        }
        return decNum;
    }
}
