package b6_chuoi_nguoc;

public class printReverseString {

    public static String reverseString(String chuoiNhap) {
        // a b c -> c b a
        byte[] strAsBytes = chuoiNhap.getBytes();

        byte[] result = new byte[strAsBytes.length];

        for (int i = 0; i < strAsBytes.length; i++) {
            result[i] = strAsBytes[strAsBytes.length - i - 1];
        }

        return new String(result);
    }

    public static void main(String[] args) {
        System.out.println(reverseString("Cybersoft"));
    }
}
