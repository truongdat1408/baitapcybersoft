package b10_tong_so_nguyen_to;

import java.util.Scanner;

public class SoNguyenTo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        do {
            System.out.print("Nhập 1 số nguyên (lớn hơn 0): ");
            n = sc.nextInt();
        } while (n < 0);

        if (laSoNguyenTo(n)) {
            System.out.printf("%d là số nguyên tố!", n);
        } else {
            System.out.printf("%d không phải là số nguyên tố!", n);
        }
    }

    private static boolean laSoNguyenTo (int number) {
        if (number == 0) return false;
        for (int i = 2; i <= number/2; i++) {
            if (number % i == 0) return false;
        }
        return true;
    }
}
