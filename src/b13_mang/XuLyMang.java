package b13_mang;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class XuLyMang {
    private final static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Nhap vao so phan tu cua mang: ");
        int n = sc.nextInt();
        List<Integer> arr = new ArrayList<>();
        XuLyMang xlm = new XuLyMang();
        xlm.enterArray(arr, n);

        //Cau a
        System.out.printf("Gia tri trung binh: %.2f\n", xlm.giaTriTrungBinh(arr, n));

        //Cau b
        int [] maxMin = xlm.timMinMaxAmDuong(arr, number -> true);
        System.out.println("Gia tri lon nhat trong mang: " + maxMin[0]);
        System.out.println("Gia tri nho nhat trong mang: " + maxMin[1]);

        //Cau c
        int [] maxMinDuong = xlm.timMinMaxAmDuong(arr, number -> number >= 0);
        System.out.println("Gia tri lon nhat trong mang (so duong): " + (maxMinDuong[0] == Integer.MIN_VALUE ? "Mang khong co so duong" : maxMinDuong[0]));
        System.out.println("Gia tri nho nhat trong mang (so duong): " + (maxMinDuong[1] == Integer.MAX_VALUE ? "Mang khong co so duong" : maxMinDuong[1]));

        //Cau d
        int [] maxMinAm = xlm.timMinMaxAmDuong(arr, number -> number < 0);
        System.out.println("Gia tri lon nhat trong mang (so am): " + (maxMinAm[0] == Integer.MIN_VALUE ? "Mang khong co so am" : maxMinAm[0]));
        System.out.println("Gia tri nho nhat trong mang (so am): " + (maxMinAm[1] == Integer.MAX_VALUE ? "Mang khong co so am" : maxMinAm[1]));

        //Cau e
        System.out.println("So phan tu chan: " + xlm.doSolution(arr, x -> x % 2 == 0));
        System.out.println("So phan tu le: " + xlm.doSolution(arr, x -> x % 2 != 0));

        //Cau f
        int index, value;
        do {
            System.out.printf("Nhap chi so can them vao (0 -> %d): ", arr.size());
            index = sc.nextInt();
            System.out.print("Nhap gia tri: ");
            value = sc.nextInt();
        } while (index < 0 || index > arr.size());

        arr.add(index, value);
        xlm.printListArray(arr);

        //Cau g
        do {
            System.out.printf("Nhap chi so can xoa (0 -> %d): ", arr.size() - 1);
            index = sc.nextInt();
        } while (index < 0 || index >= arr.size());
        arr.remove(index);
        xlm.printListArray(arr);
    }

    /*
    Enter array elements
    + arr: an array you need input
    + n: number of elements of the array
     */
    private void enterArray (List<Integer> arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.printf("arr[%d] = ", i);
            arr.add(sc.nextInt());
        }
    }

    private double giaTriTrungBinh (List<Integer> arr, int n) {
        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        return 1.0 * sum / n;
    }


    /*
    return {maxValue, minValue}
    if (maxValue == Integer.MIN_VALUE) -> khong co gia tri trong mang arr theo dieuKienChung
    if (minValue == Integer.MAX_VALUE) -> khong co gia tri trong mang arr theo dieuKienChung
     */
    private int[] timMinMaxAmDuong (List<Integer> arr, AmDuong dieuKienChung) {
        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        for (int x : arr) {
            if (x > max && dieuKienChung.test(x)) max = x;
            if (x < min && dieuKienChung.test(x)) min = x;
        }
        return new int[] {max, min};
    }

    private List<Integer> doSolution (List<Integer> arr, Predicate<? super Integer> predicate) {
        return arr.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    private void printListArray (List<Integer> arr) {
        System.out.println("Mang hien tai: " + arr.stream().collect(Collectors.toList()));
    }
}
