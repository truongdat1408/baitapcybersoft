package b1_america_flag;

public class PrintFlag {
    public static void main(String[] args) {
        printAmericaFlag();
    }

    public static void printAmericaFlag () {
        PrintFlag p = new PrintFlag();

        for (byte i = 0; i < 15; i++) { //15: numbers of lines
            if (i < 9) { // 9: numbers of lines print stars
                if (i % 2 == 0) {
                    p.printStars(6);
                    p.printEquals(40);
                } else {
                    System.out.print(" ");
                    p.printStars(5);
                    System.out.print(" ");
                    p.printEquals(40);
                }
            } else {
                p.printEquals(12+40); //12: space + stars
            }
            System.out.println();
        }
    }

    private void printStars (int stars) {
        for (int i = 0; i < stars; i++) {
            System.out.print("* ");
        }
    }

    private void printEquals (int equals) {
        for (int i = 0; i < equals; i++) {
            System.out.print("=");
        }
    }
}
